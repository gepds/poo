/**
 * Classe Quadrado
 * @author George Mendonça - georgehrem@gmail.com
 * Data: 20/10/2016
 * Atualização: 26/10/2016
 */
package br.com.poo.classeabstrata;

public class Quadrado extends FiguraGeometrica {
	private double lado;
	
	
	public Quadrado(Double l) {
		this.setLado(l);
	}
	
	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}

	@Override
	public double perimetro() {
		return lado*4;
	}

	@Override
	public double area() {
		return lado*lado;
	}

	@Override
	public double volume() {
		return lado*lado*lado;
	}
}
