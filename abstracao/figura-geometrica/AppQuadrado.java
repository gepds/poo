/**
 * Classe AppQuadrado
 * @author George Mendonça - georgehrem@gmail.com
 * Data: 20/10/2016
 * Atualização: 26/10/2016
 */
package br.com.poo.classeabstrata;

public class AppQuadrado {

	public static void main(String[] args) {
		Quadrado q = new Quadrado(45.00);
		q.setNome("Quadrado");
		q.setPerimetro(q.perimetro());
		q.setArea(q.area());		
		q.setVolume(q.volume());
		// Apresentando os resultados
		System.out.println("Nome: "+q.getNome());
		System.out.println("Lado: "+q.getLado()+" centímetros.");
		System.out.println("Perímetro: "+q.getPerimetro()+" centímetros.");
		System.out.println("Área: "+q.getArea()+" centímetros quadrados.");
		System.out.println("Volume: "+q.getVolume()+" centímetros cúbicos.");
	}
}
/*
Saída no console:
	Nome: Quadrado
	Lado: 45.0 centímetros.
	Perímetro: 180.0 centímetros.
	Área: 2025.0 centímetros quadrados.
	Volume: 91125.0 centímetros cúbicos.
*/