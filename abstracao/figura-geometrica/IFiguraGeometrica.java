/**
 * Interface IFiguraGeometrica
 * @author George Mendonça - georgehrem@gmail.com
 * Data: 20/10/2016
 * Atualização: 26/10/2016
 */
package br.com.poo.classeabstrata;

public interface IFiguraGeometrica {
	/**
	 * Método para recuperar o nome da figura geométrica
	 * @return String
	 */
	public String getNome();
	/**
	 * Método para definir o nome da figura geométrica
	 * @param String n
	 * @return void
	 */
	public void setNome(String n);
	
	/**
	 * Método para recuperar o perímetro da figura geométrica
	 * @return double
	 */
	public double getPerimetro();
	/**
	 * Método para definir o perímetro da figura geométrica
	 * @param double p
	 * @return void
	 */
	public void setPerimetro(double p);	
	
	/**
	 * Método para recuperar a área da figura geométrica
	 * @return double
	 */
	public double getArea();
	/**
	 * Método para definir a área da figura geométrica
	 * @param double ar
	 * @return void
	 */
	public void setArea(double ar);
	
	/**
	 * Método para recuperar o volume da figura geométrica
	 * @return double
	 */
	public double getVolume();
	/**
	 * Método para definir o volume da figura geométrica
	 * @param double vm
	 * @return void
	 */
	public void setVolume(double vm);
}