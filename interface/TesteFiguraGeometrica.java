/**
 * Implementação de Interface - Figura Geométrica - App de teste
 * @author George Mendonça
 * Data: 22/09/2016
 * Atualização:13/10/2016
 */
class TesteFiguraGeometrica {
	public static void main(String[] args) {
		Quadrado objQuadrado = new Quadrado();
		objQuadrado.setNome("Quadrado");
		objQuadrado.setArea(25.56);
		objQuadrado.setVolume(82.04);
		
		System.out.println(objQuadrado);
		System.out.println("Figura: " + objQuadrado.getNome());
		System.out.println("Área: " + objQuadrado.getArea());
		System.out.println("Volume: " + objQuadrado.getVolume());
	}
}
