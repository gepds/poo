/**
 * Implementação de Interface - Figura Geométrica - Classe Quadrado
 * @author George Mendonça
 * Data: 22/09/2016
 * Atualização:13/10/2016
 */
class Quadrado implements IFiguraGeomatrica {
	public String nome = "";
	public double area = 0.0;
	public double volume = 0.0;
	
	public String getNome() {
		return this.nome;
	}
	public void setNome(String nm)  {
		this.nome = nm;
	}
	
	public double getArea() {
		return this.area;
	}
	public void setArea(double ar) {
		this.area = ar;
	}
	
	public double getVolume() {
		return this.volume;
	}
	public void setVolume(double vm) {
		this.volume = vm;
	}
}
