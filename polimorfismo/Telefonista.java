package br.com.poo.polimorfismo;
/**
 * Classe Telefonista (POLIMORFISMO)
 * @author george - Adaptado de "K19 Orientação a Objetos em Java" - www.k19.com.br
 * @date: 18/11/2016
 */
public class Telefonista extends Funcionario {
	private int ramal;

	public int getRamal() {
		return ramal;
	}

	public void setRamal(int ramal) {
		this.ramal = ramal;
	}
}
