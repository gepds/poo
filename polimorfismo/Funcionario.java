package br.com.poo.polimorfismo;
/**
 * Classe Funcionario (POLIMORFISMO)
 * @author george - Adaptado de "K19 Orientação a Objetos em Java" - www.k19.com.br
 * @date: 18/11/2016
 */
public class Funcionario {
	private int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
}
