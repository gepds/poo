package br.com.poo.polimorfismo;
/**
 * Classe Controle de Ponto (POLIMORFISMO)
 * @author george - Adaptado de "K19 Orientação a Objetos em Java" - www.k19.com.br
 * @date: 18/11/2016
 */
import java.text.SimpleDateFormat;
import java.util.Date;

class ControleDePonto {

	public void registraEntrada(Funcionario f) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date agora = new Date();		
		System.out.println("ENTRADA: " + f.getCodigo());
		System.out.println("DATA: " + sdf.format(agora));
	}

	public void registraSaida(Funcionario f) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date agora = new Date();
		System.out.println("SAÍDA: " + f.getCodigo());
		System.out.println("DATA: " + sdf.format(agora));
	}
}