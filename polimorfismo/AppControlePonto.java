package br.com.poo.polimorfismo;
/**
 * Aplicação Controle de Ponto (POLIMORFISMO)
 * @author george - Adaptado de "K19 Orientação a Objetos em Java" - www.k19.com.br
 * @date: 18/11/2016
 */
public class AppControlePonto {

	public static void main(String[] args) {
		// Criando um objeto da classe Gerente
		Gerente g = new Gerente();
		g.setCodigo(222);
		g.setUsuario("Fulano");
		g.setSenha("123");
		
		// Criando um objeto da classe Gerente
		Telefonista tf = new Telefonista();
		tf.setCodigo(100);
		tf.setRamal(11);
		
		ControleDePonto cp = new ControleDePonto();
		cp.registraEntrada(g);
		cp.registraSaida(g);
		cp.registraEntrada(tf);
		cp.registraSaida(tf);
	}
}
