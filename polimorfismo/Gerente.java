package br.com.poo.polimorfismo;
/**
 * Classe Gerente (POLIMORFISMO)
 * @author george - Adaptado de "K19 Orientação a Objetos em Java" - www.k19.com.br
 * @date: 18/11/2016
 */
public class Gerente extends Funcionario {
	private String usuario;
	private String senha;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
}
