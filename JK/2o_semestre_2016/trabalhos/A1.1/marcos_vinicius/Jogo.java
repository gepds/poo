//Importando as bibliotecas Random e Scanner. Random para gerar numeros aleatorios; Scanner para ler os numeros digitados
import java.util.Random;
import java.util.Scanner;



public class Jogo{//classe principal
    
    
    public static void main (String[] args){
        
        Scanner input = new Scanner(System.in);
        Random random = new Random();
        
        
        boolean acertou = false;
        int tentativas = 5;
        int NumeroSorteado = random.nextInt(10); //Gera um numero inteiro aleatorio de 0 a 10.
        long chute = 0;
        
        
       
        
        while(tentativas > 0 && acertou == false){
            
            System.out.println("Digite um numero entre 0 e 10: ");
             chute = input.nextLong();
             
             if(NumeroSorteado == chute ){
             
             System.out.println("Acertou");
             acertou = true;
             }
             else if(chute < NumeroSorteado){ //comparacao se o chute é maior ou menos que o numero sorteado
                 
                 --tentativas;
             
             System.out.println("Que tal tentar um numero maior?\n" + tentativas + " Tentativas Restantes...");
             }
             
             else if(chute > NumeroSorteado){
                 --tentativas;
             
             System.out.println("Que tal tentar um numero menor?\n" + tentativas + " Tentativas Restantes...");
             }
             
             else if ( tentativas == 0){
              System.out.println(NumeroSorteado);
             }
            
        
        }
        
  
             
      }
      
          
    } 