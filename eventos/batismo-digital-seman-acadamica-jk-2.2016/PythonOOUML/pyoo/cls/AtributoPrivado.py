# encoding: utf-8
'''
Created on 12 de nov de 2016

@author: george
'''
class AtributoPrivado:
    _salario = 8000.0
    
    def getSal(self):
        return self._salario
     
if __name__ == '__main__':
    obj = AtributoPrivado()
    print (obj.getSal())