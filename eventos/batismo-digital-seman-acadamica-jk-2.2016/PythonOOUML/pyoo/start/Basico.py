# coding: utf-8
'''
Created on 7 de nov de 2016

@author: George Mendonça

'''
# Comentáriode 1 linha
'''
Comentário de várias
linhas
'''
# Variáveis
salario = 12000.0

# Função
def minhaFuncao(s):
    print('Meu salário: %.2f')%(s)

# Chamando a função e imprimindo salario
minhaFuncao(salario)

if salario > 5000.00:
    print('Sênior')

