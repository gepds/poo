package br.com.poo.heranca;

/**  
 * Classe GeraMenu - Introdução à Orientação a Objetos com Java  
 *                   => Escopo, Herança, Polimorfismo  
 * @author George Mendonça  
 * Data: 06/09/2016  
 * Atualização: 15/09/2016 - 16/11/2016  
 */
public class GeraMenu extends Menu {
	/**
	 * Méto para gerar o Menu Vertical 
	 * @return Menu
	 */
	public String vertical() {
		return super.gera("> Menu Vertical").getTipo();
	}

	/**
	 * Méto para gerar o Menu Horizontal
	 * @return String
	 */
	public String horizontal() {
		return super.gera("> Menu Horizontal").getTipo();
	}
}