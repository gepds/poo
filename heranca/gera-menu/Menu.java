package br.com.poo.heranca;

/**  
 * Classe Menu - Superclasse da classe GeraMenu  
 * @author George Mendonça  
 * Data: 06/09/2016  
 * Atualização: 14/09/2016 - 16/11/2016  
 */
public class Menu {
	/** * Atributo para definição do tipo de menu */
	private String tipo;

	/**
	 * Método construtor - Inicializa o objeto data com a classe Object
	 * @return Menu menu
	 */
	protected Menu gera(String tipo) {
		Menu menu = new Menu();
		menu.tipo = tipo;
		return menu;
	}

	/**
	 * Método para retornar o tipo de menu definido
	 * @return String tipo
	 */
	public String getTipo() {
		return this.tipo;
	}
}