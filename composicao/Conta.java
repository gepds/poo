package br.com.poo.composicao;

public abstract class Conta {
	private double valor;
	
	void sacar(double valor) {
		this.valor = this.valor - valor;
	}
	
	void depositar(double valor) {
		this.valor = this.valor + valor;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
}
