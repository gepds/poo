package br.com.poo.composicao;

public class AppBanco {

	public static void main(String[] args) {
		Banco bb = new Banco();
		
		bb.getContaCorrente().setValor(100.0);
		bb.getContaSalario().setValor(200.0);
		bb.getContaPoucanca().setValor(300.0);
		
		System.out.println("Depósitos: ");
		bb.getContaCorrente().saldoCC();
		bb.getContaSalario().saldoCS();
		bb.getContaPoucanca().saldoCP();
		
		bb.getContaCorrente().sacar(10.0);
		bb.getContaSalario().sacar(50.0);
		bb.getContaPoucanca().sacar(100.0);
		
		System.out.println(" ");
		System.out.println("Saques de 10, 50 e 100 reais respectivamente: ");
		bb.getContaCorrente().saldoCC();
		bb.getContaSalario().saldoCS();
		bb.getContaPoucanca().saldoCP();
	}
}