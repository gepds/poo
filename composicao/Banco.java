package br.com.poo.composicao;

public class Banco {

	private ContaCorrente contaCorrente;
	private ContaPoucanca contaPoucanca;
	private ContaSalario contaSalario;

	public Banco() {
		this.setContaCorrente(new ContaCorrente());
		this.setContaPoucanca(new ContaPoucanca());
		this.setContaSalario(new ContaSalario());
	}

	public ContaCorrente getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public ContaPoucanca getContaPoucanca() {
		return contaPoucanca;
	}

	public void setContaPoucanca(ContaPoucanca contaPoucanca) {
		this.contaPoucanca = contaPoucanca;
	}

	public ContaSalario getContaSalario() {
		return contaSalario;
	}

	public void setContaSalario(ContaSalario contaSalario) {
		this.contaSalario = contaSalario;
	}
}
